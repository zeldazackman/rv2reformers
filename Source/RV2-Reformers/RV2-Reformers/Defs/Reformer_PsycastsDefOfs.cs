﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Defs
{
    [DefOf]
    public static class Reformer_PsycastsDefOfs
    {
        [MayRequireRoyalty]
        public static AbilityDef Rv2Reformers_ReformFromRemains;
        [MayRequireRoyalty]
        public static AbilityDef Rv2Reformers_ReformSelf;
    }
}
