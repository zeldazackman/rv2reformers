﻿using HarmonyLib;
using RimVore2;
using RimWorld;
using RV2_Reformers.Constants;
using RV2_Reformers.Settings;
using RV2_Reformers.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RV2_Reformers.Reform.ScheduledReforms;
using RV2_Reformers.Utilities;
using RV2_Reformers.Defs;

namespace RV2_Reformers.Reform
{
    public static class ReformUtil
    {

        internal static void NotifyReformAborted(List<ReformRecord> abortedPawns, ThingWithComps responsibleObject)
        {
            Map map = responsibleObject.Map;
            abortedPawns.ForEach(record =>
            {
                GlobalReformTracker.UnregisterReform(record);
                //Removed death notifications as they are now saved in caches
            });

            string names = string.Join(", ", abortedPawns.Select(p => p.Pawn.Name));
            NotificationUtility.DoNotification(GetSetting.AbortedNotification, StringKeys.AbortedReformList.Translate(names), StringKeys.ReformAbortedNotification, new LookTargets(responsibleObject));
        }
        internal static void OnDeleted(ReformRecord record)
        {
            NotifyDeath(record);
        }

        private static void NotifyDeath(ReformRecord record)
        {
            RV2Log.Message($"Notifying Death for {record.Pawn}", LogCategories.DeathNotifications);
            //There are multiple death notification things that are prevented in patches and we need to let them know the pawn is dead

            #region Relations
            try
            {
                var notifyPawnKilledMethod = AccessTools.Method("RimWorld.Pawn_RelationsTracker:Notify_PawnKilled");

                // Invoking the method using reflection
                if (notifyPawnKilledMethod != null)
                {
                    notifyPawnKilledMethod.Invoke(record.Pawn.relations, new object[] { null, record.Map });
                }
                else
                {
                    Log.Error("Failed to find method RimWorld.Pawn_RelationsTracker:Notify_PawnKilled");
                }
            }
            catch (Exception ex)
            {
                // Logging any exceptions that occur
                Log.Error("Exception occurred while notifying royal pawn killed in relations: " + ex.Message);
            }

            #endregion
            #region Ideo
            try
            {
                record.Pawn.Ideo?.Notify_MemberDied(record.Pawn);
            }
            catch (Exception e)
            {
                RV2Log.Warning($"Exception notifying pawn aborted for ideo member died\n{e}", LogCategories.DeathNotifications);
            }
            #endregion
            #region royalty
            try
            {
                record.Pawn.royalty?.Notify_PawnKilled();
            }
            catch (Exception e)
            {
                RV2Log.Warning($"Exception notifying pawn aborted for royalty pawn killed\n{e}", LogCategories.DeathNotifications);
            }
            #endregion
        }

        public static bool TryReformPawn(ReformRecord record, Map map, IntVec3 position)
        {
            if(record.Pawn == null) return false;
            if (!record.Pawn.Dead) return false;
            if (record.Pawn.Discarded)
            {
                RV2Log.Warning("Requested reform on discarded pawn", LogCategories.Unexpected);
            }

            bool Ressurected = ReformRessurection.TryRessurect(record, out string reason);
            if (!Ressurected)
            {
                NotificationUtility.DoNotification(GetSetting.ReformUnexpectedFailedNotification, reason.Translate(), StringKeys.UnexpectedReformFailed);
                return false;
            }
            try
            {
                ReformRessurection.ApplyRecordCache(record);
            } catch (Exception ex) {
                RV2Log.Error($"Exception while applying Record Cache.\n{ex}", LogCategories.Ressurect);
            }
            ReformRessurection.TeleportToPosition(record, map, position);

            LookTargets targets = new LookTargets(record.Pawn);
            NotificationUtility.DoNotification(GetSetting.ReformCompleteNotification, StringKeys.ReformSuccess, StringKeys.ReformNotification, targets);
            try
            {
                ClearExistingVoreTracker(record);
            } catch(Exception e)
            {
                Log.Error("Exception during try reform pawn clear existing vore tracker " + e);
            }
            try
            {
                ApplyPostReformThoughts(record);
            }
            catch (Exception e)
            {
                Log.Error("Exception during try reform pawn Applying thoughts " + e);
            }
            try
            {
                ApplyReformChip(record);
            } catch(Exception e)
            {
                Log.Error("Exception during try reform pawn apply chip" + e);
            }
            PostReformCleanup(record);

            return true;
        }

        public static bool TryReformPawn(ReformRecord record, TargetInfo target)
        {
            if (target.Thing != null && target.Thing.Map != null)
            {
                return TryReformPawn(record, target.Thing.Map, target.Thing.Position);
            }
            else if (target.Map != null && target.Cell != null)
            {
                return TryReformPawn(record, target.Map, target.Cell);
            }
            else
            {
                throw new NotImplementedException($"Target info does not match any supported reformable target cannot reform {record.Pawn.Label}");
            }
        }
        public static void ClearExistingVoreTracker(ReformRecord record)
        {
            var tracker = GlobalVoreTrackerUtility.GetVoreRecord(record.Pawn);
            if (tracker == null) return;
            var predPawnData = tracker.Predator.PawnData(false);
            if(predPawnData == null)
            {
                Log.Warning("Attempted to clear existing vore tracker. Found a tracker but predators pawn data was null");
                return;
            }
            predPawnData.VoreTracker.UntrackVore(tracker);
            ApplyPrematureThought(record);
        }

        private static void ApplyReformChip(ReformRecord record)
        {
            if (!record.HasReformChip) return;
            if (record.Pawn?.health?.hediffSet?.HasHediff(Reformers_HediffDefOfs.ReformChipHediff) != false) return;
            var part = record.Pawn.RaceProps.body.AllParts.FirstOrDefault(p => p.def == BodyPartDefOf.Torso);
            record.Pawn.health.AddHediff(Reformers_HediffDefOfs.ReformChipHediff, part);
        }

        private static void PostReformCleanup(ReformRecord record)
        {
            GlobalReformTracker.UnregisterReform(record);
            GlobalReformTracker.UnregisterRecord(record);
            Rv2ReformersFind.ReformLocationUtilComponent.RemoveContainer(record.Pawn);
        }

        #region thoughts
        private static void ApplyPostReformThoughts(ReformRecord record)
        {
            record.Pawn?.needs?.mood?.thoughts?.memories?.TryGainMemory(Reformer_ThoughtDefOfs.Rv2Reformers_Reformed);
        }

        private static void ApplyPrematureThought(ReformRecord record)
        {
            record.Pred?.needs?.mood?.thoughts?.memories?.TryGainMemory(Reformer_ThoughtDefOfs.Rv2Reformers_PreyReformedPrematuraly, record.Pawn);
        }
        #endregion
    }
}
