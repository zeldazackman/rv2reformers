﻿using RimVore2;
using RV2_Reformers.Cacher;
using RV2_Reformers.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Reform
{

    //This tracks not just active reformation but potential reformation
    public class ReformTracker : GameComponent
    {
        public static ReformTracker Instance;
        public List<ReformRecord> Records = new List<ReformRecord>();
        public Dictionary<Pawn, ReformRecord> PawnRecordMap = new Dictionary<Pawn, ReformRecord>();
        public List<Pawn> ActiveReforms = new List<Pawn>();
        public List<Pawn> PawnsInQueue = new List<Pawn>();
        public List<Pawn> SkipNextReformRequest = new List<Pawn>();

        public ReformTracker(Game game)
        {
            Instance = this;
        }
        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Collections.Look(ref Records, "Records", LookMode.Deep);
            Scribe_Collections.Look(ref ActiveReforms, "ActiveReforms", true, LookMode.Reference);
            Scribe_Collections.Look(ref PawnsInQueue, "PawnsInQueue", LookMode.Reference);
            Scribe_Collections.Look(ref SkipNextReformRequest, "SkipNextReformRequest", LookMode.Reference);
            
            PawnsInQueue = PawnsInQueue ?? new List<Pawn>();
        }
        public override void FinalizeInit()
        {
            base.FinalizeInit();
            foreach(var record in Records)
            {
                PawnRecordMap.Add(record.Pawn, record);
            }
        }
    }
    public static class GlobalReformTracker
    {
        private static ReformTracker Tracker => ReformTracker.Instance;

        public static void RegisterRecord(ReformRecord record, bool AutoAddToBestCache = true)
        {
            if (record?.Pawn == null) return;

            Tracker.Records.Add(record);
            Tracker.PawnRecordMap[record.Pawn] = record;

            if (AutoAddToBestCache) ReformCacheUtil.AddForNewRecord(record);

            RemoveExcessRecords();
        }

        public static void UnregisterRecord(ReformRecord record)
        {
            if (record?.Pawn == null || !Tracker.Records.Contains(record)) return;

            Tracker.Records.Remove(record);
            Tracker.PawnRecordMap.Remove(record.Pawn);
            ReformCacheUtil.RemoveRecord(record);
            ReformUtil.OnDeleted(record);
        }

        public static void UnregisterRecord(Pawn pawn)
        {
            if (pawn == null) return;
            UnregisterRecord(GetRecord(pawn));
        }

        public static bool HasRecord(Pawn pawn) => Tracker.PawnRecordMap.ContainsKey(pawn);

        public static ReformRecord GetRecord(Pawn pawn)
        {
            if (pawn == null) return null;
            Tracker.PawnRecordMap.TryGetValue(pawn, out var record);
            return record;
        }

        //Active reformations
        public static void RegisterReform(Pawn pawn)
        {
            RegisterReform(GetRecord(pawn));
        }
        public static void UnregisterReform(Pawn pawn)
        {
            UnregisterReform(GetRecord(pawn));
        }
        public static void RegisterReform(ReformRecord record)
        {
            Tracker.ActiveReforms.AddDistinct(record.Pawn);
        }
        public static void UnregisterReform(ReformRecord record)
        {
            Tracker.ActiveReforms.Remove(record.Pawn);
            Tracker.PawnsInQueue.Remove(record.Pawn);
        }
        public static bool IsReforming(Pawn pawn)
        {
            return Tracker.ActiveReforms.Contains(pawn);
        }
        public static bool IsReforming(ReformRecord record)
        {
            return IsReforming(record.Pawn);
        }

        internal static IEnumerable<ReformRecord> GetAllRecords() => Tracker.Records;

        public static void RemoveExcessRecords()
        {
            if (Tracker.Records.Count <= GetSetting.MaxRecords) return;

            var removableRecords = Tracker.Records
                .Where(CanSafelyRemove)
                .GroupBy(r => r.Importance)
                .OrderBy(g => g.Key)
                .SelectMany(g => g.OrderBy(r => r.CreatedTick))
                .Take(Tracker.Records.Count - GetSetting.MaxRecords)
                .ToList();

            foreach (var record in removableRecords)
            {
                UnregisterRecord(record);
            }
        }

        public static bool CanSafelyRemove(ReformRecord record) => !IsReforming(record);

        //Queue info
        #region Queue
        public static bool IsInQueue(Pawn pawn)
        {
            Tracker.PawnsInQueue = Tracker.PawnsInQueue ?? new List<Pawn>();
            return Tracker.PawnsInQueue.Contains(pawn);
        }

        public static void RegisterInQueue(Pawn pawn)
        {
            Tracker.PawnsInQueue.Add(pawn);
        }
        public static void UnregisterInQueue(Pawn pawn)
        {
            Tracker.PawnsInQueue = Tracker.PawnsInQueue.Where(p=>p != pawn).ToList();
        }
        #endregion
        #region Skip Reform
        public static void RegisterSkipReform(Pawn pawn)
        {
            Tracker.SkipNextReformRequest.Add(pawn);
        }
        public static void UnregisterSkipReform(Pawn pawn)
        {
            Tracker.SkipNextReformRequest = Tracker.SkipNextReformRequest.Where(p => p != pawn).ToList();
        }
        public static bool ShouldSkipReform(Pawn pawn)
        {
            return Tracker.SkipNextReformRequest.Contains(pawn);
        }
        #endregion
    }
}
