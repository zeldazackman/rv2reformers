﻿using RimVore2;
using RimWorld;
using RV2_Reformers.ReformLocationUtil;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Reform
{
    //Reform record keeps pawn data needed for reformation
    //This is primarly the prey but also may include the pred
    //In addtion this hold data that rimworld normally clears and false to restore on ressurection that otherwise should be save
    //Social thoughts are a in the latter category
    //This is not a store of active reformations
    public class ReformRecord : IExposable
    {
        public Pawn Pawn;
        public Pawn Pred;

        public List<Thought_Memory> CachedMemories;
        public List<Thought_Situational> CachedSituationals;

        public Map Map;

        public int CreatedTick;
        public float Importance;

        public Pawn_WorkSettings workSettings;

        public bool HasReformChip = false;

        public bool IsSetToBeReformed => IsInQueue || IsReforming;
        public bool IsReforming => GlobalReformTracker.IsReforming(Pawn);
        public bool IsInQueue => GlobalReformTracker.IsInQueue(Pawn);
         

        public void CancelReform()
        {
            if (!IsSetToBeReformed) return;
            Rv2ReformersFind.ScheduleReformationManager.Remove(this.Pawn);
            foreach(var reformer in ReformerFinder.AllPlayerReformers())
            {
                reformer.RemoveRequest(this);
            }
        }

        public void ExposeData()
        {
            Scribe_References.Look(ref Pawn, "pawn", true);
            Scribe_References.Look(ref Pred, "pred", true);
            Scribe_Collections.Look(ref CachedMemories, "Memories", LookMode.Deep);
            Scribe_Collections.Look(ref CachedSituationals, "Situational", LookMode.Deep);
            Scribe_References.Look(ref Map, "Map", true);
            Scribe_Values.Look(ref CreatedTick, "CreatedTick");
            Scribe_Values.Look(ref Importance, "Importance");
            Scribe_Deep.Look(ref workSettings, "workSettings");
            Scribe_Values.Look(ref HasReformChip, "HasReformChip", false);
        }


    }
}
