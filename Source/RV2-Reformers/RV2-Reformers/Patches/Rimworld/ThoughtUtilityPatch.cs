﻿using HarmonyLib;
using RimWorld;
using RV2_Reformers.ModCompatiblity;
using RV2_Reformers.ModCompatiblity.Vanilla;
using RV2_Reformers.ModCompatiblity.VE;
using RV2_Reformers.Reform;
using RV2_Reformers.Settings;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RV2_Reformers.Patches.RV2;
using RV2_Reformers.Defs;
using RimVore2;
using RimWorld.Planet;
using RV2_Reformers.Cacher;

namespace RV2_Reformers.Patches.Rimworld
{
    [HarmonyPatch(typeof(ThoughtUtility), nameof(ThoughtUtility.CanGetThought))]
    public static class CanGetThoughtPatch
    {
        [HarmonyPostfix]
        public static void Postfix(Pawn pawn, ThoughtDef def, bool checkIfNullified, ref bool __result)
        {
            if (!__result) return;
            if (!ApplicableToThought(pawn, def, checkIfNullified)) return;

            var prey = DigestionUtilityPatch.ActiveRecord.Prey;
            var IdeoActive = ModsConfig.IdeologyActive && pawn.Ideo != null;
            if (IdeoActive)
            {
                if (pawn.Ideo.HasPrecept(Reformer_IdeoDefOfs.Rv2Reformers_VoreDeath_Normal))
                {
                    ProcessAlwaysDeath(prey, ref __result);
                }
                else if (pawn.Ideo.HasPrecept(Reformer_IdeoDefOfs.Rv2Reformers_VoreDeath_DontCare))
                {
                    ProcessUncaring(prey, ref __result);
                }
                else if (pawn.Ideo.HasPrecept(Reformer_IdeoDefOfs.Rv2Reformers_VoreDeath_Cached))
                {
                    ProcessCached(prey, ref __result);
                }
                else if (pawn.Ideo.HasPrecept(Reformer_IdeoDefOfs.Rv2Reformers_VoreDeath_Reforms))
                {
                    ProcessReforming(prey, ref __result);
                }
                else if (pawn.Ideo.HasPrecept(Reformer_IdeoDefOfs.Rv2Reformers_VoreDeath_ReformerExists))
                {
                    ProcessReformable(prey, ref __result);
                }
                ProcessFallback(prey, ref __result);
                return;
            }
            ProcessFallback(prey, ref __result);
        }

        private static void ProcessCached(Pawn prey, ref bool __result)
        {
            __result = ReformCacheUtil.IsCached(prey);
            if (!__result) ProcessReforming(prey, ref __result);
        }

        private static void ProcessReformable(Pawn prey, ref bool __result)
        {
            __result = !(ReformerFinder.PlayerOwnedReformersOnMap(prey.Map).Any() || ReformerFinder.PlayerOwnedGlobalReformers().Any());
        }
        private static void ProcessAlwaysDeath(Pawn prey, ref bool __result)
        {
            return;//Don't have anything to do in this one but in case I want to add something later
        }
        private static void ProcessReforming(Pawn prey, ref bool __result)
        {
            __result = !GlobalReformTracker.IsReforming(prey);
        }
        private static void ProcessUncaring(Pawn prey, ref bool __result)
        {
            __result = false;
        }
        private static void ProcessFallback(Pawn prey, ref bool __result)
        {
            __result = !GlobalReformTracker.IsReforming(prey);
        }

        private static bool ApplicableToThought(Pawn pawn, ThoughtDef def, bool checkIfNullified)
        {
            if(pawn == null) return false;
            if(def == null) return false;

            //If someone wants to disable this patch for whatever reason
            if (!GetSetting.PatchThoughtsUtilityPatch) return false;

            //If they were killed by finish digestion (and thus vore) this should have a record for us
            if (DigestionUtilityPatch.ActiveRecord == null) return false;

            //dont know why this would happen but just in case
            if (DigestionUtilityPatch.ActiveRecord.Prey == null) return false;

            //If its not a death related thought no need to deal with it
            if (!ThoughtUtilityPatchHelper.DeathRelatedThoughts.Contains(def)) return false;

            return true;
        }
    }

    public static class ThoughtUtilityPatchHelper
    {
        private static List<ThoughtDef> _InvalidThoughtsAboutReformingPawns = null;
        public static List<ThoughtDef> DeathRelatedThoughts
        {
            get
            {
                if (_InvalidThoughtsAboutReformingPawns == null) Init();
                return _InvalidThoughtsAboutReformingPawns;
            }
        }

        private static void Init()
        {
            _InvalidThoughtsAboutReformingPawns = new List<ThoughtDef>();

            //Vanilla
            _InvalidThoughtsAboutReformingPawns.AddRange(new List<ThoughtDef>()
            {
                ThoughtDefOf.WitnessedDeathAlly, ThoughtDefOf.WitnessedDeathFamily, ThoughtDefOf.WitnessedDeathNonAlly, ThoughtDefOf.WitnessedDeathBloodlust,
                ThoughtDefOf.KnowColonistDied, ThoughtDefOf.KilledChild, ThoughtDefOf.KilledHumanlikeBloodlust, ThoughtDefOf.KilledMyFriend, ThoughtDefOf.KilledMyRival,
                ThoughtDefOf.PawnWithGoodOpinionDied, ThoughtDefOf.PawnWithBadOpinionDied,

                VanillaThoughtDefs.KilledColonyAnimal, VanillaThoughtDefs.KilledMyBondedAnimal, VanillaThoughtDefs.BondedAnimalDied,
                VanillaThoughtDefs.KilledColonist, 
                VanillaThoughtDefs.MyKinDied, VanillaThoughtDefs.KilledMyKin,
                VanillaThoughtDefs.KilledMyFather, VanillaThoughtDefs.MyFatherDied, VanillaThoughtDefs.KilledMyMother, VanillaThoughtDefs.MyMotherDied,
                VanillaThoughtDefs.MySonDied, VanillaThoughtDefs.KilledMySon, VanillaThoughtDefs.KilledMyDaughter, VanillaThoughtDefs.MyDaughterDied,
                VanillaThoughtDefs.MySisterDied, VanillaThoughtDefs.KilledMySister, VanillaThoughtDefs.MyBrotherDied, VanillaThoughtDefs.KilledMyBrother,
                VanillaThoughtDefs.KilledMyLover, VanillaThoughtDefs.MyLoverDied, VanillaThoughtDefs.KilledMySpouse, VanillaThoughtDefs.MyWifeDied,
                VanillaThoughtDefs.KilledMyFiance, VanillaThoughtDefs.MyFianceDied, VanillaThoughtDefs.MyFianceeDied, VanillaThoughtDefs.MyHusbandDied,
                VanillaThoughtDefs.MyGrandchildDied, VanillaThoughtDefs.MyNieceDied, VanillaThoughtDefs.MyNephewDied, VanillaThoughtDefs.MyHalfSiblingDied,
                VanillaThoughtDefs.MyAuntDied, VanillaThoughtDefs.MyUncleDied, VanillaThoughtDefs.MyGrandparentDied, VanillaThoughtDefs.MyCousinDied

            });

            //Rimvore 2
            _InvalidThoughtsAboutReformingPawns.AddRange(new List<ThoughtDef>
            {
                VoreThoughtDefOf.RV2_FatallyVoredMemory_Social, VoreThoughtDefOf.RV2_FatallyVoredMemory_Mood
                , VoreThoughtDefOf.RV2_FatallyVoredMemory_Rival_Social, VoreThoughtDefOf.RV2_FatallyVoredMemory_Rival_Mood
            });

            //Vanilla expanded
            if (ModsCheckerUtil.IsVE_SocialActive)
            {
                _InvalidThoughtsAboutReformingPawns.AddRange(new List<ThoughtDef>()
                {
                    VE_Defs.VSIE_KilledMyBestFriend, VE_Defs.VSIE_MyBestFriendDied, VE_Defs.VSIE_MyBestFriendDied_Female
                });
            }
            //Pawnmorpher
            if (ModsCheckerUtil.IsPawnmorpherActive)
            {
                _InvalidThoughtsAboutReformingPawns.AddRange(new List<ThoughtDef>()
                {
                    PawnmorpherDefs.MyMergeMateDied,PawnmorpherDefs.MyExMergeDied, PawnmorpherDefs.KilledMyExMerge, PawnmorpherDefs.KilledMyMergeMate
                });
            }


            _InvalidThoughtsAboutReformingPawns = _InvalidThoughtsAboutReformingPawns.Where(t=>t != null).Distinct().ToList();
        }
    }
}
