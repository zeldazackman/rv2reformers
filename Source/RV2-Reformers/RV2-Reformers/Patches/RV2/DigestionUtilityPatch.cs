﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.Constants;
using RV2_Reformers.Settings;
using RV2_Reformers.Debug;
using RV2_Reformers.Reform;
using System;
using System.Linq;
using Verse;
using RV2_Reformers.Defs;
using RV2_Reformers.ModCompatiblity.Immortals;
using RV2_Reformers.Utilities;

namespace RV2_Reformers.Patches.RV2
{
    [HarmonyPatch(typeof(DigestionUtility), nameof(DigestionUtility.FinishDigestion))]
    internal class DigestionUtilityPatch
    {
        public static VoreTrackerRecord ActiveRecord = null;

        //This patch is responsible for catching the pawn upon digestion for auto reformers
        [HarmonyPrefix]
        public static void Patch(VoreTrackerRecord record)
        {
            ActiveRecord = record;
            try
            {
                if (record.Prey == null)
                {
                    RV2Log.Message("FinishDigestion prey was null. Skipping reform request", LogCategories.ReformPatches);
                    return;
                }
                var reformRecord = ReformRecordFactory.MakeAndRegisterRecord(record);
                if (reformRecord == null)
                {
                    RV2Log.Message($"ReformRecordFactory returned null for {record.Prey?.Name}", LogCategories.ReformPatches);
                    return;
                }

                if (!GetSetting.DoAutoReform) return;

                if (ModsCheckerUtil.IsImmortalsActive)
                {
                    if (ImmortalsUtil.ShouldTransfer(reformRecord))
                        ImmortalsUtil.DoTransfer(reformRecord);
                }

                var quirkManager = record.Prey.PawnData()?.QuirkManager(false);
                if (!ShouldAlwaysReform(record, quirkManager))
                {
                    if (GlobalReformTracker.ShouldSkipReform(record.Prey))
                    {
                        GlobalReformTracker.UnregisterSkipReform(record.Prey);
                        return;
                    }
                    if (quirkManager != null)
                    {
                        if (quirkManager.HasQuirk(Reformers_QuirksDefOfs.Cheat_UnReformable))
                        {
                            GlobalReformTracker.UnregisterSkipReform(record.Prey);
                            return;
                        }
                    }
                }
                var RequestResult = AutoReformUtil.RequestReform(reformRecord);

                if (!RequestResult.SilentFail && !RequestResult.RequestSuccessful)
                    NotificationUtility.DoNotification(NotificationType.LetterThreatSmall, RequestResult.Reasoning.Translate(reformRecord.Pawn), StringKeys.ReformNotification, new Verse.LookTargets(RequestResult.ReformingThing));
                if (RequestResult.RequestSuccessful)
                    NotificationUtility.DoNotification(NotificationType.MessageNeutral, StringKeys.AutoReforming.Translate(reformRecord.Pawn), StringKeys.ReformNotification);
            } catch (Exception e)
            {
                RV2Log.Error($"Caught exception in DigestionUtilityPatch. \n{e}", LogCategories.ReformPatches);
            }
        }
        [HarmonyPostfix]
        public static void PostFix(VoreTrackerRecord record)
        {
            ActiveRecord = null;
        }

        public static bool ShouldAlwaysReform(VoreTrackerRecord record, QuirkManager quirkManager)
        {
            if (quirkManager == null) return false;
            if(quirkManager.HasQuirk(Reformers_QuirksDefOfs.Cheat_AlwaysReform)) return true;
            return false;
        }
    }
    [HarmonyPatch(typeof(VoreContainer), nameof(VoreContainer.FinishDigestion))]
    internal class VoreProductFinishDigestionPatch
    {
        [HarmonyPostfix]
        public static void PostFix(VoreContainer __instance)
        {
            try
            {
                var fieldInfo = AccessTools.Field(typeof(VoreContainer), "thingOwner");
                var thingOwner = fieldInfo.GetValue(__instance) as ThingOwner<Thing>;
                var things = __instance.ThingsInContainer;
                if(things == null)
                {
                    return;
                }
                var reformChips = things.Where(t => t.def == Reformers_HediffDefOfs.ReformChipHediff.spawnThingOnRemoved).ToList();
                if (!reformChips.Any()) return;
                foreach (var chip in reformChips)
                {
                    if (chip == null) continue;
                    thingOwner.Remove(chip);
                    //chip.Destroy();
                }
            } catch (Exception e) {
                Log.Message($"Reformers Exception during VoreCotnainer Finish Digestion Patch: {e}");
            }
        }
    }
}
