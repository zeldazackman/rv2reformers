﻿using RimVore2;
using RimWorld;
using RV2_Reformers.Constants;
using RV2_Reformers.Settings;
using RV2_Reformers.Debug;
using RV2_Reformers.Reform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RV2_Reformers.Cacher;

namespace RV2_Reformers.Comps
{
    public class Comp_Reformer : ThingComp
    {
        #region Component Cache
        private CompAssignableToPawn _compAssignableToPawn;
        private bool hasLoadedCompAssignableToPawn;
        protected CompAssignableToPawn AssignableToPawn => GetComp(ref _compAssignableToPawn, ref hasLoadedCompAssignableToPawn);

        private CompPowerTrader _compPowerTrader;
        private bool hasLoadedPowerTraderComponent;
        protected CompPowerTrader PowerTrader => GetComp(ref _compPowerTrader, ref hasLoadedPowerTraderComponent);

        private Comp_ReformerCache _compInternalReformerCache;
        private bool hasLoadedCompInternalReformerCache;
        protected Comp_ReformerCache InternalReformerCache => GetComp(ref _compInternalReformerCache, ref hasLoadedCompInternalReformerCache);
        #endregion

        #region Active Reformation Variables
        protected int CurrentReformingTicks = 0;
        protected Queue<Pawn> ReformQueue = new Queue<Pawn>();
        protected Pawn ReformingPawn;
        #endregion

        #region Prop Accessors
        public CompProperties_Reformer Props => (CompProperties_Reformer)props;
        public bool HasGlobalRange => Props.Global;
        public bool CanAutoReform => Props.Automatic;
        public float ReformTickMultiplier => (float)Math.Max(.001f, Props.ReformTicksMultiplier);
        #endregion

        #region Comp Properties
        public bool HasPowerTrader => PowerTrader != null;
        public bool HasAssignability => AssignableToPawn != null;
        public bool PowerOn => !HasPowerTrader || PowerTrader?.PowerOn == true;
        #endregion

        #region Combined Properties
        public bool IsAutomaticOn => CanAutoReform && AutoReformOn;
        public bool IsReforming => ReformingPawn != null || ReformQueue.Any();
        public bool AbleToAddToReformQueue => PowerOn;
        public bool IsCurrentlyAssignable => HasAssignability && AssignableToPawn.HasFreeSlot;
        public int QueueCount => ReformQueue.Count + (ReformingPawn == null ? 0 : 1);
        public IEnumerable<Pawn> ReformList => ReformingPawn != null ? new[] { ReformingPawn }.Concat(ReformQueue) : ReformQueue;
        public ReformRecord ReformingPawnRecord => GlobalReformTracker.GetRecord(ReformingPawn);
        public float Progress => CurrentReformingTicks * ReformTickMultiplier / GetSetting.ReformNormalTickRequirement;
        public int ProgressPercentage => (int)Math.Round(Progress * 100);
        #endregion

        public bool AutoReformOn = true;


        public Comp_Reformer() : base()
        {
        }

        #region Overrides
        public override void PostExposeData()
        {
            base.PostExposeData();
            Scribe_References.Look(ref ReformingPawn, "CurrentReformingRecord", true);
            Scribe_Values.Look(ref CurrentReformingTicks, "CurrentReformingTicks");

            List<Pawn> queueList = ReformQueue.ToList();
            Scribe_Collections.Look(ref queueList, "queue", true, LookMode.Reference );
            if (Scribe.mode == LoadSaveMode.ResolvingCrossRefs)
            {
                ReformQueue = new Queue<Pawn>(queueList);
            }
        }
        public override string CompInspectStringExtra()
        {
            return BuildInspectString();
        }

        public override IEnumerable<Gizmo> CompGetGizmosExtra()
        {
            foreach (Gizmo item in base.CompGetGizmosExtra()) 
                yield return item;

            if(Prefs.DevMode && IsReforming) {
                yield return CreateDebugReformNowGizmo();
            }
        }

        public override void CompTick()
        {
            base.CompTick();
            PerformReformingTick();
        }

        public override void PostDestroy(DestroyMode mode, Map previousMap)
        {
            base.PostDestroy(mode, previousMap);
            AbortAndClearQueue();
        }
        #endregion

        #region class API
        public void ReformCurrentPawn()
        {
            bool result = ReformUtil.TryReformPawn(ReformingPawnRecord, this.parent.Map, this.parent.Position);
            ReformingPawn = null;
            CurrentReformingTicks = 0; 
            ProcessNextInQueue();
        }
        public bool AddPawn(Pawn pawn)
        {
            if (ReformingPawn == null)
            {
                ReformingPawn = pawn;
                GlobalReformTracker.RegisterReform(pawn);
                return true;
            }
            else if (AbleToAddToReformQueue)
            {
                this.ReformQueue.Enqueue(pawn);
                GlobalReformTracker.RegisterInQueue(pawn);
                GlobalReformTracker.RegisterReform(pawn);
                return false;
            }
            return false;
        }
        public void RemoveRequest(ReformRecord record)
        {
            if (ReformingPawn == record.Pawn)
            {
                ReformingPawn = null;
                ProcessNextInQueue(); // Process the next pawn in the queue after removal
                GlobalReformTracker.UnregisterReform(record);
                return;
            }

            ReformQueue = new Queue<Pawn>(ReformQueue.Where(p => p != record.Pawn));
            GlobalReformTracker.UnregisterReform(record);
        }

        public IEnumerable<ReformRecord> GetAvaliableRecords()
        {
            return GetAvaliableCacheRecordLists().SelectMany(r => r).Distinct();
        }
        public IEnumerable<IEnumerable<ReformRecord>> GetAvaliableCacheRecordLists()
        {
            yield return ReformCacheUtil.GetMapCaches(this.parent.Map).SelectMany(c=>c.CachedReformRecords);
            if (this.HasGlobalRange)
                yield return ReformCacheUtil.GetGlobalCaches().SelectMany(c => c.CachedReformRecords); ;
        }

        #endregion

        #region Abort Reform
        private bool AbortCheck()
        {
            if (ReformingPawn == null) return false;
            if (ReformingPawn.Dead != true)
            {
                RV2Log.Message($"Reforming aborted due to {ReformingPawn.LabelShort} is not dead.", LogCategories.ReformerStatus);
                ReformingPawn = null;
                return true;
            }
            if (HasPowerTrader && !PowerOn)
            {
                AbortAndClearQueue();
                return true;
            }
            return false;
        }
        private void AbortAndClearQueue()
        {
            List<ReformRecord> AbortedPawns = ReformList.Select(p => GlobalReformTracker.GetRecord(p)).ToList();

            ReformUtil.NotifyReformAborted(AbortedPawns, this.parent);

            ReformingPawn = null;
            ReformQueue.Clear();
        }
        #endregion

        #region UI
        private string BuildInspectString()
        {
            var builder = new StringBuilder();
            builder.AppendLine(GetStatus());
            return builder.ToString().Trim();
        }
        private string GetStatus()
        {
            if (!IsReforming) return StringKeys.StandByKey.Translate();
            if (ReformingPawn == null) return StringKeys.ReformingLoading.Translate();
            return ReformQueue.Any()
                ? StringKeys.ReformProgressWithQueue.Translate(
                    ReformingPawn.LabelShort,
                    ProgressPercentage,
                    string.Join(", ", ReformQueue.Select(p => p.Name)))
                : StringKeys.ReformingProgress.Translate(ReformingPawn.LabelShort, ProgressPercentage);
        }
        private Command_Action CreateDebugReformNowGizmo()
        {
            return new Command_Action()
            {
                defaultLabel = StringKeys.DebugReformNow.Translate(),
                action = ReformCurrentPawn
            };
        }

        #endregion

        #region Tick
        private void PerformReformingTick()
        {
            if(ReformingPawn == null && !ReformQueue.Any()) return;
            if (ReformingPawn == null)
            {
                ProcessNextInQueue();
            }

            if (AbortCheck()) return;

            CurrentReformingTicks++;

            if (Progress >= 1.0f)
                ReformCurrentPawn();
        }

        private void ProcessNextInQueue()
        {
            if (!ReformQueue.Any()) return;
            ReformingPawn = ReformQueue.Dequeue();
            GlobalReformTracker.UnregisterInQueue(ReformingPawn);
            CurrentReformingTicks = 0;
        }
        #endregion

        #region Class Util
        private T GetComp<T>(ref T compField, ref bool loadedFlag) where T : ThingComp
        {
            if (!loadedFlag)
            {
                compField = this.parent.TryGetComp<T>();
                loadedFlag = true;
            }
            return compField;
        }
        #endregion
    }
}
