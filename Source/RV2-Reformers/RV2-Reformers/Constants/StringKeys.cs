﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Constants
{
    public static class StringKeys
    {
        public const string InvalidDueToExistingRecordKey = "RV2Reformer-ExistingRecord";

        public const string StandByKey = "RV2Reformer-OnStandBy";
        public const string ReformingProgress = "RV2Reformer-ReformingProgress";
        public const string ReformProgressWithQueue = "RV2Reformer-ReformingProgressWithQueue";
        public const string UnlimitedQueue = "RV2Reformer-UnlimitedQueue";
        public const string QueueCount = "RV2Reformer-QueueCount";
        public const string ReformingLoading = "RV2Reformer-ReformingLoading";
        public const string DebugReformNow = "RV2Reformer-DebugReformNow";
        public const string ReformAbortedNotification = "RV2Reformer-ReformAbortedNotification";
        public const string AbortedReformList = "RV2Reformer-ReformPawnListAbortedText";
        public const string RessurectionException = "RV2Reformer-RessurectionException";
        public const string NullPawn = "RV2Reformer-UnexpectedNullPawn";
        public const string UnexpectedReformFailed = "RV2Reformer-UnexpectedReformFailed";
        public const string ReformSuccess = "RV2Reformer-ReformSuccess";
        public const string ReformNotification = "RV2Reformer-ReformNotification";
        public const string AutoReforming = "RV2Reformer-AutoReformingSuccess";
        public const string AlreadyReforming = "RV2Reformer-FailAlreadyReforming";
        public const string RequestFailNonPlayerFaction = "RV2Reformer-RequestFailNonPlayerFaction";
        public const string RequestFailMissingReformer = "RV2Reformer-MissingReformer";
        public const string RequestFailMissingCachedRecord = "RV2Reformer-RequestFailMissingCachedRecord";
        public const string ReformRequestFail_NoSelfReformationAvaliable = "RV2Reformer-ReformRequestFail_NoSelfReformationAvaliable";
        public const string ReformButton = "RV2Reformer-ReformButton";
        public const string ReformInProgress = "RV2Reformer-ReformInProgress";
        public const string ReformInQueue = "RV2Reformer-ReformInQueue";
        public const string FullQueue = "RV2Reformer-FullQueue";
        public const string ToggleRefomerAutoReform = "RV2Reformer-ReformerAutoReforms";
        public const string Tab_ReformRequestSelect = "RV2Reformer-Tab_ReformRequestSelect";
        public const string Tab_CacheView = "RV2Reformer-Tab_CacheView";
        public const string Delete = "RV2Reformer-Delete";
        public const string Cancel = "RV2Reformer-Cancel";
        public const string CacheUnlimited = "RV2Reformer-CacheUnlimited";
        public const string CacheTotal = "RV2Reformer-CacheTotal";

        //Setting keys
        public const string SettingsGeneral = "RV2Reformer-SettingsGeneral";
        public const string SettingsPatches = "RV2Reformer-SettingsPatches";

    }
}
