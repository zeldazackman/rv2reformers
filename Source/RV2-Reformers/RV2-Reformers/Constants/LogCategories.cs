﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Debug
{
    public static class LogCategories
    {
        public const string ReformPatches = "RV2Reformers-Patches";
        public const string Unexpected = "RV2Reformers-Unexpected";
        public const string General = "RV2Reformers";
        public const string ReformerStatus = "RV2Reformers-ReformerStatus";
        public const string ReformAbortions = "RV2Reformers-ReformAbortions";
        public const string DeathNotifications = "RV2Reformers-DeathNotifications";
        public const string Ressurect = "RV2Reformers-Ressurection";
        public const string ReformRecordFactory = "RV2Reformers-ReformRecordFactory";
        public const string UI = "RV2Reformers-UI";
        public const string SettingsSetup = "RV2SettingSetup";
        public const string SettingsScribe = "RV2SettingScribe";
        public static string ScheduleReform = "Rv2Reformers-ScheduleReformers";
    }
}
