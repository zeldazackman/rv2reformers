﻿using Immortals;
using RV2_Reformers.Reform;
using RV2_Reformers.Settings;
using RV2_Reformers.Utilities;
using System.Reflection;
using Verse;

namespace RV2_Reformers.ModCompatiblity.Immortals
{
    public static class ImmortalsUtil
    {
        public static bool ShouldAutoReform(ReformRecord reformRecord)
        {
            if (!ModsCheckerUtil.IsImmortalsActive) return false;
            if (GetSetting.ImmortalsReformationOption == ImmortalsReformationOption.None) return false;
            return reformRecord.Pawn.IsImmmortal();
        }
        public static bool ShouldTransfer(ReformRecord reformRecord)
        {
            if (!ModsCheckerUtil.IsImmortalsActive) return false;
            if(reformRecord.Pred == null) return false;
            if (GetSetting.ImmortalsTransferOption == ImmortalsTransferOption.None) return false;
            if (!reformRecord.Pawn.IsImmmortal()) return false;
            if (GetSetting.ImmortalsTransferOption == ImmortalsTransferOption.TransferToKnownImmortals && !reformRecord.Pred.IsVisibleImmortal()) return false;
            if (GetSetting.ImmortalsTransferOption == ImmortalsTransferOption.TransferToImmortals && !reformRecord.Pred.IsImmmortal()) return false;

            return GetSetting.ImmortalsTransferOption == ImmortalsTransferOption.AlwaysTransfer || reformRecord.Pred.IsImmmortal();
        }

        internal static void DoTransfer(ReformRecord reformRecord)
        {
            var prey = reformRecord.Pawn;
            var pred = reformRecord.Pred;
            if(prey == null || pred == null) return;

            if (pred.health == null) return;

            var preyImmortal = prey.GetImmortalHediff();
            if (preyImmortal == null) return;//this shouldn't happen

            var predImmortal = pred.GetImmortalHediff();
            Immortal_Component ImmortalComp = Current.Game.GetComponent<Immortal_Component>();
            var QuickingMethodInfo = typeof(Immortal_Component).GetMethod("DoQuickening", BindingFlags.NonPublic | BindingFlags.Instance);
            if (QuickingMethodInfo == null) {
                Log.Error("Reformers Immortal util tried to call DoQuickening but the method info was null");
                return;
            }

            if (predImmortal == null)
            {
                HediffDef hediff = HediffDefOf_Immortals.IH_Immortal;
                if (pred.Dead) ImmortalComp.AddDeadImmortal(pred);
                else ImmortalComp.AddImmortal(pred, false);
                pred.health.AddHediff(hediff);
            }

            QuickingMethodInfo.Invoke(ImmortalComp, new object[] { prey, pred });

        }
    }
}
