﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.ModCompatiblity.Vanilla
{
    [DefOf]
    public static class VanillaThoughtDefs
    {
        public static ThoughtDef BondedAnimalDied;
        public static ThoughtDef MyKinDied;
        public static ThoughtDef MyFatherDied, MyMotherDied;
        public static ThoughtDef MySonDied, MyDaughterDied;
        public static ThoughtDef MySisterDied, MyBrotherDied;
        public static ThoughtDef MyLoverDied, MyFianceDied, MyFianceeDied;
        public static ThoughtDef MyHusbandDied, MyWifeDied;

        public static ThoughtDef MyGrandchildDied;
        public static ThoughtDef MyNieceDied;
        public static ThoughtDef MyNephewDied;
        public static ThoughtDef MyHalfSiblingDied;
        public static ThoughtDef MyAuntDied;
        public static ThoughtDef MyUncleDied;
        public static ThoughtDef MyGrandparentDied;
        public static ThoughtDef MyCousinDied;

        public static ThoughtDef KilledColonyAnimal;
        public static ThoughtDef KilledColonist;
        public static ThoughtDef KilledMyBondedAnimal;
        public static ThoughtDef KilledMyKin;
        public static ThoughtDef KilledMyFather, KilledMyMother;
        public static ThoughtDef KilledMySon, KilledMyDaughter;
        public static ThoughtDef KilledMyBrother, KilledMySister;
        public static ThoughtDef KilledMySpouse, KilledMyFiance, KilledMyLover;
    }
}
