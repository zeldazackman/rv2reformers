﻿using RimVore2;
using RV2_Reformers.ModCompatiblity.Immortals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Settings
{
    //This is used to get the settings for the code and is not the settings menu in rimworld
    public static class GetSetting
    {
        public static bool PatchIdeoMemberDeath => ReformerSettings.Patches.PatchIdeoMemberDeath;
        public static bool PatchVanillaExpandedPawnKilled => ReformerSettings.Patches.PatchVanillaExpandedPawnKilled;
        public static bool PatchPlayerDeathNotification => ReformerSettings.Patches.PatchPlayerDeathNotification;
        public static bool PatchRelationTracker => ReformerSettings.Patches.PatchRelationTracker;
        public static bool PatchThoughtsUtilityPatch => ReformerSettings.Patches.PatchThoughtsUtility;

        public static int ReformNormalTickRequirement => ReformerSettings.General.BaseReformTickRequirement;
        public static NotificationType AbortedNotification => ReformerSettings.General.ReformAbortedNotification;
        public static NotificationType ReformUnexpectedFailedNotification => ReformerSettings.General.ReformUnexpectedFailedNotification;
        public static NotificationType ReformCompleteNotification => ReformerSettings.General.ReformCompleteNotification;
        public static bool DoAutoReform => ReformerSettings.General.DoAutoReform;
        public static int MaxRecords => ReformerSettings.General.MaxReformRecords;

        public static int MinSelfReformTime => ReformerSettings.General.MinSelfReformTime;
        public static int MaxSelfReformTime => ReformerSettings.General.MaxSelfReformTime;

        #region immortals
        public static ImmortalsReformationOption ImmortalsReformationOption => ReformerSettings.General.ImmortalsReformationOption;
        public static ImmortalsTransferOption ImmortalsTransferOption => ReformerSettings.General.ImmortalsTransferOption;
        #endregion
    }
}
