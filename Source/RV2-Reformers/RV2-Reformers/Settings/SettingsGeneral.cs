﻿using RimVore2;
using RV2_Reformers.ModCompatiblity;
using RV2_Reformers.ModCompatiblity.Immortals;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_Reformers.Settings
{
    public class SettingsTab_ReformerGeneral : SettingsTabFillable
    {
        public SettingsTab_ReformerGeneral(string label, Action clickedAction, bool selected) : base(label, clickedAction, selected)
        {
        }
        public SettingsTab_ReformerGeneral(string label, Action clickedAction, Func<bool> selected) : base(label, clickedAction, selected)
        {
        }

        public override SettingsContainerFillable FillableContainer => (SettingsContainerFillable)ReformerSettings.General; 
    }

    public class SettingsContainer_ReformerGeneral : SettingsContainerFillable
    {
        public int MinSelfReformTime => (int)_MinSelfReformTime.value;
        public int MaxSelfReformTime => (int)_MaxSelfReformTime.value;

        public int BaseReformTickRequirement => (int)_BaseReformTickRequirement.value;
        public int MaxReformRecords => (int)_MaxReformRecords.value;
        public bool DoAutoReform => _DoAutoReform.value;
        public NotificationType ReformAbortedNotification => _ReformAbortedNotification.value;
        public NotificationType ReformUnexpectedFailedNotification => _ReformUnexpectedFailedNotification.value;
        public NotificationType ReformCompleteNotification => _ReformCompleteNotification.value;

        private FloatSmartSetting _MinSelfReformTime;
        private FloatSmartSetting _MaxSelfReformTime;

        private FloatSmartSetting _BaseReformTickRequirement;
        private FloatSmartSetting _MaxReformRecords;
        private BoolSmartSetting _DoAutoReform;
        private EnumSmartSetting<NotificationType> _ReformAbortedNotification;
        private EnumSmartSetting<NotificationType> _ReformUnexpectedFailedNotification;
        private EnumSmartSetting<NotificationType> _ReformCompleteNotification;

        #region Immortals
        private EnumSmartSetting<ImmortalsReformationOption> _ImmortalsReformationOption;
        public ImmortalsReformationOption ImmortalsReformationOption => _ImmortalsReformationOption.value;
        private EnumSmartSetting<ImmortalsTransferOption> _ImmortalTransferOption;
        public ImmortalsTransferOption ImmortalsTransferOption => _ImmortalTransferOption.value;
        #endregion

        private bool heightStale = true;
        private float height = 0f;
        private Vector2 scrollPosition;


        public override void FillRect(Rect inRect)
        {
            Rect outerRect = inRect;
            UIUtility.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Listing_Standard list);
            try
            {
                if (list.ButtonText("RV2Reformer-Settings_Reset".Translate()))
                    Reset();

                _MinSelfReformTime.DoSetting(list);
                _MaxSelfReformTime.DoSetting(list);

                _BaseReformTickRequirement.DoSetting(list);
                _MaxReformRecords.DoSetting(list);
                _DoAutoReform.DoSetting(list);
                _ReformAbortedNotification.DoSetting(list);
                _ReformUnexpectedFailedNotification.DoSetting(list);
                _ReformCompleteNotification.DoSetting(list);

                if (ModsCheckerUtil.IsImmortalsActive)
                {
                    _ImmortalsReformationOption.DoSetting(list);
                    _ImmortalTransferOption.DoSetting(list);
                }

            } catch(Exception e)
            {
                Widgets.TextArea(outerRect, e.Message, true);
                EnsureSmartSettingDefinition();
            }

            list.EndScrollView(ref height, ref heightStale);
        }

        public override void Reset()
        {
            _BaseReformTickRequirement = null;
            _MaxReformRecords = null;
            _DoAutoReform = null;
            _ReformAbortedNotification = null;
            _ReformUnexpectedFailedNotification = null;
            _ReformCompleteNotification = null;
            _MaxSelfReformTime = null;
            _MinSelfReformTime = null;

            if (ModsCheckerUtil.IsImmortalsActive)
            {
                _ImmortalsReformationOption = null;
                _ImmortalTransferOption = null;
            }

            EnsureSmartSettingDefinition();
        }
        public override void ExposeData()
        {
            base.ExposeData();
            if(Scribe.mode == LoadSaveMode.Saving || Scribe.mode == LoadSaveMode.LoadingVars)
            {
                EnsureSmartSettingDefinition();
            }
            Scribe_Deep.Look(ref _MaxSelfReformTime, "MaxSelfReformTime", new object[0]);
            Scribe_Deep.Look(ref _MinSelfReformTime, "MinSelfReformTime", new object[0]);
            Scribe_Deep.Look(ref _BaseReformTickRequirement, "BaseReformTickRequirement", new object[0]);
            Scribe_Deep.Look(ref _MaxReformRecords, "MaxReformRecords", new object[0]);
            Scribe_Deep.Look(ref _DoAutoReform, "DoAutoReform", new object[0]);
            Scribe_Deep.Look(ref _ReformAbortedNotification, "ReformAbortedNotification", new object[0]);
            Scribe_Deep.Look(ref _ReformUnexpectedFailedNotification, "ReformUnexpectedFailedNotification", new object[0]);
            Scribe_Deep.Look(ref _ReformCompleteNotification, "ReformCompleteNotification", new object[0]);
            Scribe_Deep.Look(ref _ImmortalsReformationOption, "_ImmortalsReformationOption", new object[0]);
            Scribe_Deep.Look(ref _ImmortalTransferOption, "_ImmortalTransferOption", new object[0]);

        }
        public override void EnsureSmartSettingDefinition()
        {
            if(_BaseReformTickRequirement?.IsInvalid() != false)
                _BaseReformTickRequirement = new FloatSmartSetting("Settings_BaseReformTickRequirement", 60000, 60000, 1000, 420000, "SettingsTooltip_BaseReformTickRequirement", "0");
            if (_MaxReformRecords?.IsInvalid() != false)
                _MaxReformRecords = new FloatSmartSetting("Settings_MaxReformRecords", 50, 50, 10, 200, "SettingsTooltips_MaxReformRecords", "0");

            if (_MaxSelfReformTime?.IsInvalid() != false)
                _MaxSelfReformTime = new FloatSmartSetting("Settings_MaxSelfReformTime", 60000 * 2, 60000 * 2, 6000, 60000 * 7, "SettingsTooltips_MaxSelfReformTime", "0");
            if (_MinSelfReformTime?.IsInvalid() != false)
                _MinSelfReformTime = new FloatSmartSetting("Settings_MinSelfReformTime", 60000 * .5f, 60000 * .5f, 6000, 60000 * 7, "SettingsTooltips_MinSelfReformTime", "0");

            if (_DoAutoReform?.IsInvalid() != false) 
                _DoAutoReform = new BoolSmartSetting("Settings_DoAutoReform", true, true, "SettingsTooltip_DoAutoReform");
            
            if (_ReformAbortedNotification?.IsInvalid() != false)
                _ReformAbortedNotification = new EnumSmartSetting<NotificationType>("Settings_ReformAbortedNotification", NotificationType.LetterThreatSmall, NotificationType.LetterThreatSmall, "SettingsTooltip_ReformAbortedNotification");
            if (_ReformUnexpectedFailedNotification?.IsInvalid() != false)
                _ReformUnexpectedFailedNotification = new EnumSmartSetting<NotificationType>("Settings_ReformUnexpectedFailedNotification", NotificationType.LetterThreatSmall, NotificationType.LetterThreatSmall, "SettingsTooltip_ReformUnexpectedFailedNotification");
            if (_ReformCompleteNotification?.IsInvalid() != false)
                _ReformCompleteNotification = new EnumSmartSetting<NotificationType>("Settings_ReformCompleteNotification", NotificationType.MessageNeutral, NotificationType.MessageNeutral, "SettingsTooltip_ReformCompleteNotification");
            if (ModsCheckerUtil.IsImmortalsActive)
            {
                if(_ImmortalsReformationOption?.IsInvalid() != false)
                    _ImmortalsReformationOption = new EnumSmartSetting<ImmortalsReformationOption>("Settings_ImmortalsReformationOption", ImmortalsReformationOption.None, ImmortalsReformationOption.None, "SettingsTooltip_ImmortalsReformationOption");
                if(_ImmortalTransferOption?.IsInvalid() != false)
                    _ImmortalTransferOption = new EnumSmartSetting<ImmortalsTransferOption>("Settings_ImmortalTransferOption", ImmortalsTransferOption.None, ImmortalsTransferOption.None, "SettingsTooltip_ImmortalTransferOption");
            }
        }
    }
}
