﻿using HarmonyLib;
using RV2_Reformers.Patches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Core
{
    [StaticConstructorOnStartup]
    public class StartUp
    {
        public static readonly string Name = "RV2_Reformers";
        static StartUp()
        {
            Harmony harmony = new Harmony(Name);
            
            var allTypes = Assembly.GetExecutingAssembly().GetTypes();
            foreach (var type in allTypes)
            {
                var harmonyPatchAttribute = type.GetCustomAttribute<HarmonyPatch>();
                var patchRequiresModAttribute = type.GetCustomAttribute<PatchRequiresMod>();
                if (harmonyPatchAttribute == null && patchRequiresModAttribute == null) continue;
                if(patchRequiresModAttribute != null)
                {
                    if(!ModsConfig.IsActive(patchRequiresModAttribute.Mod)) continue;
                }
                harmony.CreateClassProcessor(type).Patch();
            }
        }
    }
}
